package util

import (
	"bufio"
	"log"
	"os"

	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"

	bp "github.com/nexustix/boilerplate"
)

//HACK FIXME rewrite
func NxwrapWriterSTDIO(pipe *chan nxwrap.Message) func() {
	h := bufio.NewWriter(os.Stdout)
	//mux := NewMuxBasic()
	mux := nxwrap.Multiplexer{}
	return func() {
	loop:
		for {
			select {
			case msg := <-*pipe:
				mux.PushMessage(msg)
				_, err := h.Write(mux.Pop())
				//log.Printf("<wrt< %s \n", mMsg)
				h.Flush()
				if bp.GotError(err) {
					log.Printf("<!> CRITICAL fail writing to STDOUT >%s<", err)
					break loop
				}
			}
		}
	}
}

//HACK FIXME rewrite
func NxwrapReaderSTDIO(pipe *chan nxwrap.Message) func() {
	h := bufio.NewReader(os.Stdin)
	var msg nxwrap.Message
	var success bool

	//demux := NewDemuxBasic()
	demux := nxwrap.Demultiplexer{}
	return func() {
		for {
			buff := make([]byte, 1024)
			n, err := h.Read(buff)
			//log.Printf(">red> %s \n", buff[0:n])
			if bp.GotError(err) {
				log.Printf("<!> CRITICAL fail STDIN reading >%s<\n", err)
			} else {
				//log.Printf(">>%s<<\n", buff[0:n])

				//demux.DemultiplexBytes(buff[0:n])
				demux.Push(buff[0:n])
			loop:
				for {
					msg, success = demux.Pop()
					if success {
						*pipe <- msg
					} else {
						break loop
					}
				}
			}
		}
	}
}
