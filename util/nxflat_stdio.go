package util

import (
	"bufio"
	"log"
	"os"

	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxflat"
	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"

	bp "github.com/nexustix/boilerplate"
)

func NxflatWriterSTDOUT(pipe *chan nxwrap.Message) {
	h := bufio.NewWriter(os.Stdout)

	mux := nxflat.Multiplexer{}
	// var buffer []byte

	go func() {
		for {
			select {
			case msg := <-*pipe:
				mux.PushMessage(msg)
				_, err := h.Write(mux.Pop())
				//log.Printf("<wrt< %s \n", mMsg)
				h.Flush()
				if bp.GotError(err) {
					log.Printf("<!> CRITICAL fail writing to STDOUT >%s<", err)
					return
				}
			}
		}
	}()
}

func NxflatReaderSTDIN(pipe *chan nxwrap.Message) {
	h := bufio.NewReader(os.Stdin)
	demux := nxflat.Demultiplexer{}
	var msg nxwrap.Message
	var success bool
	go func() {
		for {
			buff := make([]byte, 1024)
			n, err := h.Read(buff)
			if bp.GotError(err) {
				log.Printf("<!> CRITICAL fail STDIN reading >%s<\n", err)
				return
			} else {
				demux.Push(buff[0:n])
			}
		loop:
			for {
				msg, success = demux.Pop()
				if success {
					*pipe <- msg
				} else {
					break loop
				}
			}
		}
	}()
}
