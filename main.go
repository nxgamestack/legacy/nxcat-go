package main

import (
	"flag"
	"fmt"

	"gitlab.com/nxgamestack/nxcat/nxcat-go/util"
	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"
	"gitlab.com/nxgamestack/nxnet/nxnet-go/tcpBasic"
	"gitlab.com/nxgamestack/nxnet/nxnet-go/tcpTlsBasic"
)

type Server interface {
	Listen()
	Send()
	GetSendChannel() chan nxwrap.Message
	GetReceiveChannel() chan nxwrap.Message
}

func main() {
	hostnamePtr := flag.String("hostname", "0.0.0.0", "adress to accept connections from")
	portPtr := flag.String("port", "8080", "nxcat port")
	sslPtr := flag.Bool("ssl", false, "use ssl")
	interfacePtr := flag.String("interface", "nxwrap", "protocol for STDIO (nxwrap, nxflat)")

	flag.Parse()

	var server Server

	if *sslPtr {
		server = tcpTlsBasic.NewServer(*hostnamePtr, *portPtr, "nxcat-tcp-tls")
	} else {
		server = tcpBasic.NewServer(*hostnamePtr, *portPtr, "nxcat-tcp")
	}

	go server.Listen()
	go server.Send()
	serverSend := server.GetSendChannel()
	serverReceive := server.GetReceiveChannel()

	pipeSend := make(chan nxwrap.Message, 8)
	pipeReceive := make(chan nxwrap.Message, 8)

	switch *interfacePtr {
	case "nxwrap":
		// FIXME uses old code
		swriter := util.NxwrapWriterSTDIO(&pipeSend)
		sreader := util.NxwrapReaderSTDIO(&pipeReceive)
		go swriter()
		go sreader()
	case "nxflat":
		util.NxflatWriterSTDOUT(&pipeSend)
		util.NxflatReaderSTDIN(&pipeReceive)
	default:
		panic(fmt.Sprintf("Unknown protocol \"%v\"", *interfacePtr))
	}

	for {
		select {
		case msg := <-pipeReceive:
			serverSend <- msg
		case msg := <-serverReceive:
			pipeSend <- msg

		}
	}

}

//(cheese dat 1 whatever)
//(cheese dat 4 the cake is a lie)
//(cheese dat 42 the quick browm fox jumps over the lazy dog)
//(cheese dat 0 testing)
